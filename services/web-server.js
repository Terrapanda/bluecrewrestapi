
const bodyParser = require('body-parser')
const express = require('express');
const http = require('http');
const morgan = require('morgan');
const passport = require('passport');
const cors = require('cors');
const { to, ReE }  = require('../services/util.js');
const pe            = require('parse-error');''

const router = require('./router.js');
const webServerConfig = require('../config/web-server.js');

let httpServer;

function initialize() {
  return new Promise((resolve, reject) => {
    const app = express();
    httpServer = http.createServer(app);

    app.use(bodyParser.urlencoded({
      extended: true
    }));

    app.use(bodyParser.json())
    app.use(morgan('combined'));
    app.use(passport.initialize());
    app.use(cors());
    app.use('/api', router);

    // app.use('/', function(req, res){
    //    res.statusCode = 200; // send the appropriate status code
    //    res.json({status:"success", message:"Cat Pride API", data:{"Version": "v1.0.0"}})
    // });


    // catch 404 and forward to error handler
    app.use(function(req, res, next) {
      var err = new Error('Not Found');
      err.status = 404;
      next(err);
    });

    // error handler
    app.use(function(err, req, res, next) {
      ReE(res, err, 500)
      next()
    });

    // This is here to handle uncaught promise rejections
    process.on('unhandledRejection', error => {
        console.error('Uncaught Error', pe(error));
    });


    httpServer.listen(webServerConfig.port)
      .on('listening', () => {
        console.log(`Web server listening on localhost:${webServerConfig.port}`);

        resolve();
      })
      .on('error', err => {
        reject(err);
      });
  });
}

function close() {
  return new Promise((resolve, reject) => {
    httpServer.close((err) => {
      if (err) {
        reject(err);
        return;
      }

      resolve();
    });
  });
}

module.exports.close = close;
module.exports.initialize = initialize;
