CONFIG = {}

CONFIG.jwt_algorithm=process.env.JWT_ALGORITHM || 'HS256';
CONFIG.jwt_encryption=process.env.JWT_ENCRYPTION || 'needmorecatlovers';
CONFIG.jwt_expiration=process.env.JWT_EXPIRATION || '10000';

CONFIG.HTTP_PORT=3000

CONFIG.KITTY_USER="kitty"
CONFIG.KITTY_PASSWORD="bluecrew"
CONFIG.KITTY_CONNECTIONSTRING="localhost"
CONFIG.KITTY_CONNECTION_LIMIT="10"
CONFIG.KITTY_DATABASE="bluecrew_cats"

CONFIG.invalid_login_credential = "The username and password combination you entered is incorrect. Please try again."

module.exports = CONFIG

//
// connectionLimit: 10,
// database: "bluecrew_cats",
// host: process.env.KITTY_CONNECTIONSTRING || "localhost",
// password: process.env.KITTY_PASSWORD || "bluecrew",
// user: process.env.KITTY_USER || "kitty",
