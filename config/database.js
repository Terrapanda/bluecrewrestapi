const CONFIG = require('../config/config');

module.exports = {
  catPool: {
    connectionLimit: 10,
    database: "bluecrew_cats",
    host: CONFIG.KITTY_CONNECTIONSTRING || "localhost",
    password: CONFIG.KITTY_PASSWORD || "bluecrew",
    user: CONFIG.KITTY_USER || "kitty",
  }
};
