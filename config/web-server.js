const CONFIG = require('../config/config');

module.exports = {
  port: CONFIG.HTTP_PORT || 3000
};
