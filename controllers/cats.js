const { authUser, getJWT } = require('../services/auth.js');
const { ReE, ReS, TE, to } = require('../services/util.js');

const getCatQuery = require('../db_apis/cats.js');

function getCatFromRecord(_body) {
  const body = Object.assign({}, _body);
  return {
    birthdate: body.birthdate,
    breed: body.breed,
    imageURL: body.imageURL,
    name: body.name,
    password: body.password,
    username: body.username,
    weight: body.weight,
  };
}

function checkValidSearchQuery(_query) {
  const query = Object.assign({}, _query)

  delete query.id;
  delete query.limit;
  delete query.sort;
  delete query.name;
  delete query.username;

  if (Object.keys(query).length > 0)
    return false
  return true;
}
async function getRandom (req, res, next) {
  let [err, row] = await to (getCatQuery.findRandom());
  if (err)
    return next(err.message);

  return ReS(res, {cats: row[0]}, 200)
}

async function get(req, res, next) {
  res.setHeader('Content-Type', 'application/json');
  const context = { search: {} };

  const pass = checkValidSearchQuery(req.query);
  if (!pass)
    next('Query parameters invalid, please search by [name, username, id] with options to [limit, sort]')

  context.id = parseInt(req.params.id, 10);
  context.sort = req.query.sort;
  context.limit = parseInt(req.query.limit, 10);
  context.search.id = parseInt(req.query.id, 10);
  context.search.name = req.query.name;
  context.search.username = req.query.username;

  let [err, rows] = await to (getCatQuery.find(context));
  if (err)
    next(err.message);

  if (!Number.isNaN(context.id)) {
    if (rows.length === 0)
      return res.status(404);

    return ReS(res, {cats: rows[0]}, 200)
  }

  if (rows.length === 0)
    return ReS(res, {message: "No cats found."}, 404)

  return ReS(res, {cats: rows}, 200)
}

async function login(req, res, next) {


  let [err, userAndToken] = await to (authUser(req.body, next));
  if(err) return ReE(res, err, 422);

  let result;
  [err, result] = await to(getCatQuery.lastSeen(userAndToken.user.id));
  if(err) next(err);

  return ReS(res, userAndToken);
}


async function register(req, res, next) {
  res.setHeader('Content-Type', 'application/json');

  if (!req.body.password || !req.body.username)
    next('Please be sure to enter a valid username and password to register.');

  const usernameExists = await getCatQuery.findUserByUsername(String(req.body.username));
  if(usernameExists)
    next('This username already exists, please login or try again.')

  if (String(req.body.username).length > 32)
    next('Please enter a username no more than 32 characters.');

  if (req.body.preHashPass.length < 8)
    next('Please be sure your password is atleast 8 characters.');

  if (!req.body.name)
    next('Please be sure your cat has a name. Suggestions: Atlas, Juno, Odin, or Oggy')

  if (!req.body.weight)
    next('Imaginary cats? For medical reasons, please include the weight of your animal.')

  let err;
  let record = getCatFromRecord(req.body);
  [err, record] = await to (getCatQuery.register(record));
  if (err) return ReE(res, err, 422);
  const token = await getJWT(record);
  return ReS(res, {message:'Successfully register a new cat.', cats:record, token}, 201);
}

async function update(req, res, next) {
    const id = parseInt(req.params.id, 10);

    let err;
    let record = getCatFromRecord(req.body);
    [err, record] = await to (getCatQuery.update(record, id));
    if (err)
      return ReE(res, err, 422);

    if (record === null) {
      return ReS(res, {message: `Cat could not be found`}, 404);
    }

    return ReS(res, {message: `Cat "${record.name}" has been updated`}, 200)
}

async function del(req, res, next) {
    const id = parseInt(req.params.id, 10);

    let err, success;
    let [err, success] = to (await getCatQuery.delete(id));

    if (cat === null)
      return ReS(res, {message: `Cat could not be found`}, 404);

    return ReS(res, {message: `Cat "${cat.name}" has been deleted`}, 204);
}

module.exports.delete = del;
module.exports.update = update;
module.exports.register = register;
module.exports.get = get;
module.exports.getRandom = getRandom;
module.exports.login = login;
